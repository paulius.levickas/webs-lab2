# Product storage

## Launch the app

Follow steps below in order to launch the application:

1. ```docker-compose up -d``` - to build and run the application.

## How to use the api

### GET - get all products

```aidl
curl --location --request GET 'http://localhost:80/products' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'

```

### GET - get one product by id

```
curl --location --request GET 'http://localhost:5000/products/1' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### DELETE - delete product

```aidl
curl --location --request DELETE 'http://localhost:5000/products/4' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### POST - create product with employee

```aidl
curl --location --request POST 'http://localhost:80/products' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name": "Lova2",
	"inStock": 34,
	"price": 54.33,
	"attributes": [
		{
		"name": "aukstis",
		"value": "32cm"
		},
		{
		"name": "plotis",
		"value": "160cm"
		}
		],
		"employee": {
			"firstName": "Paulius",
			"lastName": "kazkas",
			"email": "paulius@whoond.com"
		}
}'
```

### PUT - update product

```aidl
curl --location --request PUT 'http://localhost:80/products/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 1,
    "name": "Iphone 7",
    "price": 55.55,
    "inStock": 0,
    "attributes": [
        {
            "id": 1,
            "name": "Aukštis",
            "value": "117mm"
        },
        {
            "id": 3,
            "name": "Storis",
            "value": "16mm"
        },
        {
            "id": 2,
            "name": "Plotis",
            "value": "78mm"
        }
    ],
    "employee": "paulius.paulius@gmail.com"
}'
```

### PATCH - patch product

```aidl
curl --location --request PATCH 'http://localhost:80/products/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "employee": "paulius@gmail.com"
}'
```

### POST - create employee

```$xslt
curl --location --request POST 'http://localhost:80/employees' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstName": "Paulius",
    "lastName": "Levickas",
    "email": "paulius@gmail.com"
}
'
```

### GET - get employee by email

```$xslt
curl --location --request GET 'http://localhost:80/employees/paulius@gmail.com' \
--header 'Content-Type: application/json'
```

### GET - get all employees

```$xslt
curl --location --request GET 'http://localhost:80/employees' \
--header 'Content-Type: application/javascript' \
--data-raw ''
```

author Paulius Levickas