INSERT INTO product (id, name, price, in_stock) VALUES
(1, 'Iphone 7', 55.55, 30),
(2, 'Samsung tv U54345', 55.55, 30),
(3, 'Komoda Svetlana', 55.55, 30),
(4, 'Dantų pasta "Colgate"', 55.55, 30);

INSERT INTO attributes (id, name, value, product_id) VALUES
(1, 'Aukštis', '117mm', 1),
(2, 'Plotis', '78mm', 1),
(3, 'Storis', '16mm', 1),
(4, 'Ekrano tipas', 'QLED', 2),
(5, 'Korupas', 'LMDP plokštė', 3),
(6, 'Nuodinga', 'Taip', 4);
