package com.paulius.levickas.api.controllers;

import com.paulius.levickas.api.controllers.beans.ProductBean;
import com.paulius.levickas.api.models.Product;
import com.paulius.levickas.api.models.User;
import com.paulius.levickas.api.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ProductController {

    private final ProductService productService;

    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public Object getAllProducts( @RequestParam(required = false) String expand) {
        return productService.getAllProducts(expand);
    }

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody final ProductBean productBean) {
        return productService.createProduct(productBean);
    }

    @GetMapping("/products/{productId}")
    public Object findProductById(@PathVariable final Long productId, @RequestParam(required = false) String expand) {
        return productService.findProductById(productId, expand);
    }

    @GetMapping("/products/{productId}/users")
    public User findUserByProductId(@PathVariable final Long productId) {
        return productService.findUserByProductById(productId);
    }

    @PutMapping("/products/{productId}")
    public Product updateProduct(
            @PathVariable final Long productId,
            @RequestBody final Product product) {
        return productService.updateProduct(productId, product);
    }

    @PatchMapping("/products/{productId}")
    public Product pathProduct(
            @PathVariable final Long productId,
            @RequestBody final Product product) {
        return productService.updateProduct(productId, product);
    }

    @DeleteMapping("/products/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable final Long productId) {
        productService.deleteProduct(productId);
        return ResponseEntity.ok("");
    }
}
