package com.paulius.levickas.api.controllers;

import com.paulius.levickas.api.models.Product;
import com.paulius.levickas.api.models.User;
import com.paulius.levickas.api.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAllEmployees() {
        return userService.getAllUsers();
    }

    @GetMapping("/{email}")
    public User getEmployeeByEmail(@PathVariable final String email) {
        return userService.getUserByEmail(email);
    }

    @PostMapping
    public ResponseEntity<User> createEmployee(@RequestBody final User user) {
        return userService.createUser(user);
    }

}
