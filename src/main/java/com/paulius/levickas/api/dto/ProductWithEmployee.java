package com.paulius.levickas.api.dto;

import com.paulius.levickas.api.models.Attributes;
import com.paulius.levickas.api.models.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class ProductWithEmployee implements Serializable {
    private Long id;
    private String name;
    private Double price;
    private int inStock;
    private Set<Attributes> attributes;
    private User employee;
}
