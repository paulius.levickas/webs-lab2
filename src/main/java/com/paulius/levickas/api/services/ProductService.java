package com.paulius.levickas.api.services;

import com.paulius.levickas.api.controllers.beans.ProductBean;
import com.paulius.levickas.api.dto.ProductWithEmployee;
import com.paulius.levickas.api.exceptions.ProductDuplicateException;
import com.paulius.levickas.api.exceptions.ProductNotFoundException;
import com.paulius.levickas.api.exceptions.UserNotFoundException;
import com.paulius.levickas.api.models.Product;
import com.paulius.levickas.api.models.User;
import com.paulius.levickas.api.models.UserResponse;
import com.paulius.levickas.api.repositories.ProductRepository;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.paulius.levickas.api.constants.Urls.BASE_USERS_URI;

@Service
@Log4j2
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Object getAllProducts(final String expand) {
        if(expand == null || expand.isEmpty()) {
            return productRepository.findAll();
        } else {
            List<Product> products = productRepository.findAll();
            List<ProductWithEmployee> productWithEmployees = new ArrayList<>();
            for (Product product : products) {
                ProductWithEmployee productWithEmployee = new ProductWithEmployee();
                BeanUtils.copyProperties(product, productWithEmployee);
                productWithEmployee.setEmployee(getEmployee(product.getEmployee()));
                productWithEmployees.add(productWithEmployee);
            }
            return productWithEmployees;
        }
    }

    public User findUserByProductById(final Long productId) {
        Product product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
        return getEmployee(product.getEmployee());
    }

    public ResponseEntity<Product> createProduct(final ProductBean productBean) {
        Optional<Product> existingProduct = productRepository.findByName(productBean.getName());
        if (existingProduct.isPresent()) {
            throw new ProductDuplicateException();
        }

        Product product = new Product();
        BeanUtils.copyProperties(productBean, product);
        product.setEmployee(productBean.getEmployee().getEmail());

        Product dbProduct = productRepository.save(product);
        ProductBean responseProduct = new ProductBean();
        BeanUtils.copyProperties(dbProduct, responseProduct);
        responseProduct.setEmployee(saveEmployee(productBean.getEmployee()).getData());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location",
                "/products/" + dbProduct.getId());

        try {
            return ResponseEntity.created(new URI("/products/" + dbProduct.getId())).headers(responseHeaders).body(product);
        } catch (Exception e) {
            throw new RuntimeException("Failed creating response");
        }
    }

    public Object findProductById(final Long productId, final String expand) {
        if(expand == null || expand.isEmpty()) {
            return productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
        } else {
            Product product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
            ProductWithEmployee productWithEmployee = new ProductWithEmployee();
            BeanUtils.copyProperties(product, productWithEmployee);
            productWithEmployee.setEmployee(getEmployee(product.getEmployee()));
            return productWithEmployee;
        }
    }

    public Product updateProduct(final Long productId, final Product product) {
        Product dbProduct = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
        copyNonNullProperties(product, dbProduct);
        return productRepository.save(dbProduct);
    }

    public ResponseEntity<String> deleteProduct(final Long productId) {
        if(!productRepository.existsById(productId)) {
            throw new ProductNotFoundException();
        }
        productRepository.deleteById(productId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public void copyNonNullProperties(Object source, Object destination){
        BeanUtils.copyProperties(source, destination,
                getNullPropertyNames(source));
    }

    private UserResponse saveEmployee(User user) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<User> request = new HttpEntity<>(user);
        try {
            return restTemplate.postForObject(BASE_USERS_URI, request, UserResponse.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            return UserResponse.builder().data(null).build();
        }
    }

    private User getEmployee(final String email) {
        if(email != null && !email.isEmpty()) {
            RestTemplate restTemplate = new RestTemplate();
            try {
                UserResponse userResponse = restTemplate.getForObject(BASE_USERS_URI + "/" + email, UserResponse.class);
                return userResponse.getData();
            } catch (Exception e) {
                throw new UserNotFoundException();
            }
        } else {
            return null;
        }
    }

    private String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
