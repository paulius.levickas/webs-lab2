package com.paulius.levickas.api.services;

import com.paulius.levickas.api.exceptions.ProductNotFoundException;
import com.paulius.levickas.api.models.User;
import com.paulius.levickas.api.models.UserResponse;
import com.paulius.levickas.api.models.UsersResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.paulius.levickas.api.constants.Urls.BASE_USERS_URI;

@Service
@Log4j2
public class UserService {

    public List<User> getAllUsers() {
        return getEmployees();
    }

    private List<User> getEmployees() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            UsersResponse usersResponse = restTemplate.getForObject(BASE_USERS_URI, UsersResponse.class);
            return usersResponse.getData();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ResponseEntity<User> createUser(final User user) {
        UserResponse userResponse = saveEmployee(user);
        User dbUser = userResponse.getData();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location", "/employees/" + dbUser.getEmail());

        try {
            return ResponseEntity
                    .created(new URI("/employees/" + user.getEmail()))
                    .headers(responseHeaders)
                    .body(dbUser);
        } catch (Exception e) {
            throw new RuntimeException("Failed creating response");
        }
    }

    private UserResponse saveEmployee(User user) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<User> request = new HttpEntity<>(user);
        try {
            return restTemplate.postForObject(BASE_USERS_URI, request, UserResponse.class);
        } catch (Exception e) {
            throw new RuntimeException("User creation failed");
        }
    }

    public User getUserByEmail(final String email) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate
                    .getForObject(BASE_USERS_URI + "/" + email, UserResponse.class)
                    .getData();
        } catch (Exception e) {
            throw new RuntimeException("Calling external api failed");
        }
    }
}
