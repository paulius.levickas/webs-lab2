package com.paulius.levickas.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Product already exists")
public class ProductDuplicateException extends RuntimeException {
    public ProductDuplicateException() {
    }
}
